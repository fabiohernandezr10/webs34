
from Conexion_db import Conexion_db
class Crud:
    #la clase encargada de contener las queries 
    # y ejecutarlas a traves de la clase conexion_db

    def __init__(self,mi_host,database,user,passwd):
        self.conn = Conexion_db(mi_host,database,user,passwd)#creo el objeto conn como atributo
        #el objeto conn es un objeto de la clase Conexion_db y un atributo de la clase
        #Crud
    
    #se definen los metodos para escribir, leer, actualizar tablas 

    #para tabla pasajero .... 
    def insertar_pasajero(self,nombre, direccion, telefono, fecha_nacimiento):
        query = "INSERT INTO \"Pasajeros\""+\
        "(nombre, direccion, telefono, fecha_nacimiento)"+\
            "VALUES ('"+nombre+"','"+direccion+\
                "','"+telefono+"','"+fecha_nacimiento+"')"
    
        self.conn.escribir_db(query)

    def leer_pasajeros(self):
        query= "SELECT * from \"Pasajeros\""
        pasajeros = self.conn.consultar_db(query)
        return pasajeros
    
    def eliminar_pasajero(self,id):
        query = "DELETE FROM \"Pasajeros\""+\
            "WHERE id="+str(id)
        self.conn.escribir_db(query)

    def leer_buses(self):
        query= "SELECT * from \"Buses\" ORDER BY id ASC"
        buses = self.conn.consultar_db(query)
        return buses
    
    def leer_bus(self,id):
        query= "SELECT * from \"Buses\" WHERE id="+id
        bus = self.conn.consultar_db(query)
        return bus

    def eliminar_bus(self,id):
        query= "DELETE from \"Buses\" WHERE id="+id
        print(query)
        self.conn.escribir_db(query)

    def update_bus(self,id,modelo,capacidad,placa,marca):
        query= "UPDATE \"Buses\"" +\
            " SET modelo="+modelo+ ", capacidad="+capacidad +\
                ",placa='"+placa+"', marca='"+marca+\
                    "' WHERE id="+id+";" 
        self.conn.escribir_db(query)
        print(query)

    def insertar_bus(self,modelo,capacidad,placa,marca):
        query = "INSERT INTO \"Buses\"("+\
            "modelo, capacidad, placa, marca)"+\
                "VALUES ( "+modelo+","+capacidad+",'"+placa+\
                    "','" +marca+"');"
        self.conn.escribir_db(query)

    def leer_facturas(self):
        query= "SELECT * from \"Factura\""
        facturas = self.conn.consultar_db(query)
        return facturas
    def close(self):
        self.conn.cerrar_db()
    """
    def actualizar_pasajero...
    ...
    ..
    #para la tabla buses
    def insertar_bus(self, ):
        ...
        ..
    """