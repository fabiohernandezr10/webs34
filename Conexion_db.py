import psycopg2 

class Conexion_db:
    def __init__(self,mi_host,database,user,passwd):
        try:
            #fruto de la conexión se crea el objeto conn como atributo(self.conn)
            self.conn = psycopg2.connect(
                host = mi_host,
                database=database,
                user=user,
                password=passwd) #se conecta a la db con las credenciales dadas en los parametros
            
            self.cur = self.conn.cursor()#se crea el objeto cur como atributo
        except:#si no logra hacer la conexión salta el error
            print("Error en la conexion")

    def consultar_db(self,query):
        #metodo usado para queries que retornan datos (SELECT)
        #como parametro esta la query que se va a ejecutar (SELECT * from ...)
        try:
            self.cur.execute(query)#el objeto cur ejecuta la query
            response= self.cur.fetchall()#para leer lo que nos retorna la query
            return response #devuelvo lo leido
        except(Exception, psycopg2.DatabaseError) as error:
            print(error) 
            return "error"
    
    def escribir_db(self,query):
        #metodo usado para queries que no retornan datos
        # INSERT, DELETE, CREATE
        try:
            self.cur.execute(query) #se ejecuta
            self.conn.commit()#se actualiza la db
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)
    
    def cerrar_db(self):
        #cerrar la conexion
        self.cur.close()
        self.conn.close()