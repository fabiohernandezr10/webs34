from flask import Flask, request,make_response,redirect,render_template
from flask_sqlalchemy import SQLAlchemy
from Crud import Crud
import json 
import funciones_rutas
import os

app = Flask(__name__) #instancia de flask


#postgresql://<nombre_usuario>:<password>@<host>:<puerto>/<nombre_basededatos>
app.config['SQLALCHEMY_DATABASE_URI']= 'postgresql://postgres:admin@localhost:5432/transportes'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(32)
database =SQLAlchemy(app)

from models.bus import * 

@app.route('/')
def index():
    user_ip,variable2,lista_buses=funciones_rutas.index()
    print(Trayecto.example_join())
    return render_template("index.html",ip=user_ip, v2 = variable2,buses=lista_buses)
    
@app.route('/login',methods=['GET','POST'])
def login():
    if(request.method== 'GET'):
        return render_template('login.html')
    if(request.method=='POST'):
        nombreUsuario = request.form.get("username")
        passwd = request.form.get("passwd")
        print("nombre de usuario= "+nombreUsuario+" passwd="+passwd)
        return render_template('login.html')

@app.route('/hello')
def hello():
    user_ip=request.cookies.get('user_ip')
    return "la ip del PC es: "+str(user_ip)

@app.route('/division')
def division():
    div = 10/5
    return "la division es: "+str(div)

@app.route('/multiplicar')
def multiplicar():
    mult = 10*5    
    return "la multiplicacion es-+ : "+str(mult)

@app.route('/ejercicio')
def ejercicio():
    numero = int(input("ingrese un numero: "))
    return "el cuadrado del numero-es: "+str(numero**2)

@app.route('/get_bus')
def get_bus():
    cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
    "df8er5c6o7382b",
    "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
    buses = cr.leer_buses() #buses es una lista de tuplas, cada registro es una tupla
    primer_bus = buses[0] #tomo el primer registro (la primera tupla)
    print(primer_bus)
    respuesta = json.dumps( {"id": primer_bus[0],
     "modelo": primer_bus[1], "capacidad":primer_bus[2],
     "placa": primer_bus[3], "fabricante":primer_bus[4]})
    cr.close()#cerrando la conexion 
    """
    primer_bus_string ="id: "+str(primer_bus[0])+" modelo: "+str(primer_bus[1]) +\
        "capacidad: "+str(primer_bus[2])+\
            " placa: "+primer_bus[3] + "fabricante: "+primer_bus[4]
    """
    return respuesta


@app.route("/get_buses")
def get_buses():    
    """ 
    cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
    "df8er5c6o7382b",
    "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
    buses = cr.leer_buses() #buses es una lista de tuplas, cada registro es una tupla
    cr.close()
    lista_buses=[]
    for bus in buses:#bus es la tupla de cada registro 
        lista_buses.append({"id": bus[0],
     "modelo": bus[1], "capacidad":bus[2],
     "placa": bus[3], "fabricante":bus[4]})
    #respuesta = json.dumps(lista_buses)
    """
    #Bus.update_model(1,1990)
   
    return render_template("mostrar_buses.html",buses=Bus.get_all())

@app.route("/show_edit_bus_form",methods=['GET','POST'])
def show_edit_bus_form():
    if(request.method=='POST'):
        id_bus=request.form.get("id_bus")
        print("el id del bus es: ",id_bus)
        cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
        "df8er5c6o7382b",
        "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
        bus = cr.leer_bus(id_bus) #buses es una lista de una tupla
        cr.close()
        bus = bus[0]
        print(bus)
        dic_bus = {"id":bus[0],"modelo":bus[1],"capacidad":bus[2],"placa":bus[3],"marca":bus[4]}
        return render_template("edit_bus.html",bus=dic_bus)

@app.route("/edit_bus",methods=['GET','POST'])
def edit_bus():
    if(request.method=='POST'):
        id=request.form.get("id")
        modelo=request.form.get("modelo")
        capacidad=request.form.get("capacidad")
        placa=request.form.get("placa")
        marca=request.form.get("marca")
        print("id=",id," modelo=",modelo, " capacidad=",capacidad, " placa=",placa," marca=",marca)
        cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
        "df8er5c6o7382b",
        "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
        cr.update_bus(id,modelo,capacidad,placa,marca)
        cr.close()
        response= make_response(redirect('/get_buses'))
        return response

@app.route("/delete_bus",methods=['GET','POST'])
def delete_bus():

    if(request.method=='POST'):
        id=request.form.get("id_bus")
        """        
        cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
        "df8er5c6o7382b",
        "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
        cr.eliminar_bus(id)
        cr.close()
        """
        Bus.delete_by_id(id)
        response= make_response(redirect('/get_buses'))
        return response

@app.route("/form_add_bus")
def form_add_bus():
    return render_template('add_bus.html')

@app.route("/add_bus",methods=['GET','POST'])
def add_bus():
    if(request.method=='POST'):
        modelo=request.form.get("modelo")
        capacidad=request.form.get("capacidad")
        placa=request.form.get("placa")
        marca=request.form.get("marca")
        """
        print("id=",id," modelo=",modelo, " capacidad=",capacidad, " placa=",placa," marca=",marca)
        cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
        "df8er5c6o7382b",
        "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
        cr.insertar_bus(modelo,capacidad,placa,marca)
        cr.close()
        """
        bus_to_add = Bus(modelo,capacidad,placa,marca)
        bus_to_add.create()
        response= make_response(redirect('/get_buses'))
        return response


@app.route("/test")
def test():
    cr = Crud("ec2-52-202-152-4.compute-1.amazonaws.com",
    "db6omgi47jan3t",
    "eypvpsixrfnhzk","46d17a119e8e521722cf3d7230e46d9ffa1db51c9fc4d5384256fb0fb9bb930b")
    facturas = cr.leer_facturas() #buses es una lista de tuplas, cada registro es una tupla
    print(facturas)
    return "ok"

if __name__=="__main__":
    #punto de partida de ejecucion del programa    
    print("arrancando servidor...")
    app.run(debug=True,host="0.0.0.0")
