
from os import stat
from server import database

class Bus(database.Model):
    __tablename__='Buses'

    id = database.Column(database.Integer,primary_key=True)
    modelo = database.Column(database.Integer, nullable=False)
    capacidad = database.Column(database.Integer, nullable=False)
    placa = database.Column(database.String, nullable=False)
    marca = database.Column(database.String, nullable=False)

    def __init__(self, modelo,capacidad,placa,marca):
        self.modelo=modelo
        self.capacidad=capacidad
        self.placa=placa
        self.marca=marca

    def create(self):
        database.session.add(self)
        database.session.commit()

    @staticmethod
    def delete(id_to_delete):
        Bus.query.filter_by(id=id_to_delete).delete()
        database.session.commit()

    @staticmethod 
    def update_model(id_to_update,new_model):
        bus = Bus.query.get(id_to_update)
        bus.modelo = new_model
        database.session.commit()
        
    @staticmethod
    def get_all():
        return Bus.query.all()
    @staticmethod
    def get_model(model):
        return Bus.query.filter_by(modelo=model)

 
class Trayecto(database.Model): 
    __tablename__ = "trayectos"
    id = database.Column(database.Integer,primary_key=True)
    id_estacion = database.Column(database.Integer,database.ForeignKey('Estaciones.id'))
    id_bus = database.Column(database.Integer,database.ForeignKey('Buses.id'))
   
    @staticmethod
    def get_all():
        return Trayecto.query.all()
    
    @staticmethod
    def example_join():
        #inner join
        resultado = database.session.query(Bus, Trayecto).join(Trayecto, Bus.id==Trayecto.id_bus).all()
        for tupla in resultado:
            bus = tupla[0]
            trayecto = tupla[1]

            print("bus.placa="+bus.placa+ " trayecto.id_bus="+str(trayecto.id_bus))

        #isouter
        resultado = database.session.query(Bus, Trayecto).join(Trayecto, Bus.id==Trayecto.id_bus,isouter=True).all()
        for tupla in resultado:
            bus = tupla[0]
            trayecto = tupla[1]
            if(bus!=None and trayecto!=None):
                print("bus.placa="+bus.placa+ " trayecto.id_bus="+str(trayecto.id_bus))
            elif(bus!=None):
                print("bus.placa="+bus.placa+"trayecto.id_bus=None")
            elif(trayecto!=None):
                print("bus.placa=None "+ "trayecto.id_bus="+str(trayecto.id_bus))
            else:
                print("bus.placa=None "+"trayecto.id_bus=None")